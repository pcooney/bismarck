﻿using System;
using System.Collections.Generic;

namespace Bismarck
{
    /// <summary>
    /// A set of performance measurements.
    /// </summary>
    public interface IMeasurements
    {
        /// <summary>
        /// Identifies the time the measurements were taken.
        /// </summary>
        DateTime Started { get; }

        /// <summary>
        /// The elapsed time.
        /// </summary>
        decimal ElapsedMicros { get; }

        /// <summary>
        /// True if garbage collection was triggered.
        /// </summary>
        bool DidGC { get; }

        /// <summary>
        /// The number of the core that (probably) started executing the measured action.
        /// </summary>
        int ProcessorNumberAtStart { get; }

        /// <summary>
        /// The number of the core that (probably) finished executing the measured action.
        /// </summary>
        int ProcessorNumberAtEnd { get; }

        /// <summary>
        /// A collection of measures describing various aspects of performance and CLR internals.
        /// </summary>
        IEnumerable<IMeasurement> Measurements { get; }
    }
}
