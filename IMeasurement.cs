﻿using System;

namespace Bismarck
{
    /// <summary>
    /// Some performance measurement.
    /// </summary>
    public interface IMeasurement
    {
        /// <summary>
        /// The name of the measurement.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The value before the measured action.
        /// </summary>
        decimal ValueAtStart { get; }

        /// <summary>
        /// The value after the measured action.
        /// </summary>
        decimal ValueAtEnd { get; }
    }
}
