﻿using System.Diagnostics;

namespace Bismarck
{
    internal class ElapsedMicros : ISamplingMeasure
    {
        private Stopwatch s;

        public ElapsedMicros()
        {
            PreJitStopwatch();
            s = new Stopwatch();
        }

        private static void PreJitStopwatch()
        {            
            Stopwatch sw = new Stopwatch();
            sw.Start();
            sw.Stop();
            sw.Reset();
        }

        public void SampleStart()
        {
            s.Start();
        }

        public void SampleEnd()
        {
            s.Stop();
        }

        public string Name
        {
            get { return "Elapsed microseconds";  }
        }

        public decimal ValueAtStart
        {
            get { return 0; }
        }

        public decimal ValueAtEnd
        {
            get { return 1000 * 1000M * s.ElapsedTicks / Stopwatch.Frequency; }
        }
    }
}
