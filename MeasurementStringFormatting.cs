﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bismarck
{
    internal static class MeasurementStringFormatting
    {
        const string startCat = "**************** ";
        static string endCat = " " + startCat.Substring(0, startCat.Length - 1);
        private const string lineFormat = "{0:0.##}\t{1:0.##}\t{2:0.##}\t{3}";

        internal static string Format(ElapsedMicros elapsed, Processor processor, IEnumerable<GCCount> gcs, 
            IEnumerable<PerformanceCounterSample> counters)
        {
            StringBuilder s = new StringBuilder();
            //Format the non-performance counter measures
            s.Append(FormatElapsed(elapsed));
            s.AppendLine(FormatGC(gcs));
            s.AppendLine(FormatProcessor(processor));
            //And use special formatting for the perf counters
            s.Append(FormatCounterMeasures(counters));

            return s.ToString();
        }

        private static string FormatElapsed(ElapsedMicros elapsed)
        {
            StringBuilder s = new StringBuilder();
            s.AppendLine();
            s.AppendLine(startCat + "Elapsed Time" + endCat);
            s.AppendLine(string.Format("{0:0.#} micros", elapsed.ValueAtEnd));
            return s.ToString();
        }

        private static string FormatGC(IEnumerable<GCCount> gcs)
        {
            StringBuilder s = new StringBuilder();
            s.AppendLine();
            s.AppendLine(startCat + "# Collections" + endCat);
            foreach (GCCount gc in gcs)
            {
                s.AppendLine(FormatGC(gc));
            }
            return s.ToString();
        }

        private static string FormatProcessor(Processor processor)
        {
            StringBuilder s = new StringBuilder();
            s.AppendLine();
            s.AppendLine(startCat + "Processor number" + endCat);
            s.AppendLine(string.Format("Before: {0}\tAfter: {1}", processor.ValueAtStart, processor.ValueAtEnd));
            return s.ToString();
        }

        private static string FormatGC(ISamplingMeasure gc)
        {
            return string.Format("{0} {1}", gc.ValueAtEnd - gc.ValueAtStart, gc.Name);
        }

        private static string FormatCounterMeasures(IEnumerable<PerformanceCounterSample> counterSamples)
        {
            string header = HeaderLine();

            StringBuilder s = new StringBuilder();
            if (counterSamples.Any())
            {
                counterSamples.OrderBy(x => x.Category);
                string cat = null;
                foreach (PerformanceCounterSample pc in counterSamples)
                {
                    if (pc.Category != cat)
                    {
                        cat = pc.Category;
                        s.AppendLine();
                        s.AppendLine(startCat + cat + endCat);
                        s.AppendLine(header);
                    }
                    s.AppendLine(Format(pc));
                }
            }
            return s.ToString();
        }

        private static string Format(ISamplingMeasure s)
        {
            return string.Format(lineFormat, s.ValueAtStart, s.ValueAtEnd, s.ValueAtEnd - s.ValueAtStart, s.Name);
        }

        private static string HeaderLine()
        {
            return string.Format(lineFormat, "Before", "After", "Change", "Name");
        }

    }
}
