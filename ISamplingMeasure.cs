﻿namespace Bismarck
{
    internal interface ISamplingMeasure : IMeasurement
    {
        void SampleStart();
        void SampleEnd();
    }
}
