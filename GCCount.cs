﻿using System;

namespace Bismarck
{
    /// <summary>
    /// The .NET CLR Memory / # Gen X Collections counters have information about the number of collections
    /// per generation. However, performance Counters allocate (NextValue()). This class tracks the number of
    /// collections per generation, but via a managed API, avoiding the performance counter allocations.
    /// </summary>
    internal class GCCount : ISamplingMeasure
    {
        private int gen;

        public GCCount(int generation)
        {
            if (generation < 0 || generation > 2) throw new ArgumentException("generation");
            gen = generation;
        }

        public void SampleStart()
        {
            ValueAtStart = GC.CollectionCount(gen);
        }

        public void SampleEnd()
        {
            ValueAtEnd = GC.CollectionCount(gen);
        }

        public string Name
        {
            get { return string.Format("Gen {0} garbage collections", gen); }
        }

        public decimal ValueAtStart
        {
            get;
            private set;
        }

        public decimal ValueAtEnd
        {
            get;
            private set;
        }
    }
}
