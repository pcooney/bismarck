﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Bismarck
{
    internal class ClrMeasurements : IMeasurements
    {
        /*
         List of samples used for starting and stopping. Order is important:
            - Samples are started from 0 .. n
            - Stopped from n .. 0
         This allows some samples to be tighter to the actual execution (arbitrarily the ones I am most
         interested in).
        */
        private List<ISamplingMeasure> samples;

        //Samples that are used in convenience properties ElapsedMicros and DidGC
        private GCCount gc0, gc1, gc2;
        private ElapsedMicros elapsed;
        private Processor processor;

        private const string NET_CLR_MEMORY = ".NET CLR Memory";
        private string[] perfCounterCategories = new[] 
        { 
            ".NET CLR LocksAndThreads",
            ".NET CLR Data", 
            ".NET CLR Exceptions",
            ".NET CLR Interop",
            ".NET CLR Networking",
            ".NET CLR Loading",
            ".NET CLR Jit",
            NET_CLR_MEMORY, 
        };

        [DebuggerStepThrough]
        internal ClrMeasurements()
        {
            samples = new List<ISamplingMeasure>();
            CreateProcessorMeasurement();
            CreatePerfCounterMeasures();
            CreateCollectionMeasures();
            CreateElapsedMeasure();
        }

        [DebuggerStepThrough]
        internal void SampleStart()
        {
            Started = DateTime.Now;
            foreach (var s in samples) s.SampleStart();
        }

        [DebuggerStepThrough]
        internal void SampleEnd()
        {
            //.Reverse() allocates, so better to iterate with an index
            for (int i = samples.Count - 1; i >= 0; i--) samples[i].SampleEnd();
        }

        public DateTime Started 
        { 
            get; 
            private set;  
        }

        public override string ToString()
        {
            return MeasurementStringFormatting.Format(elapsed, processor, new[] { gc0, gc1, gc2 }, samples.OfType<PerformanceCounterSample>());
        }

        public IEnumerable<IMeasurement> Measurements
        {
            get { return samples.AsReadOnly(); }
        }

        public decimal ElapsedMicros
        {
            get
            {
                return elapsed.ValueAtEnd;
            }
        }

        public bool DidGC
        {
            get
            {
                return ValueChanged(gc0) || ValueChanged(gc1) || ValueChanged(gc2);
            }
        }

        public int ProcessorNumberAtStart
        {
            get
            {
                return Convert.ToInt32(processor.ValueAtStart);
            }
        }

        public int ProcessorNumberAtEnd
        {
            get
            {
                return Convert.ToInt32(processor.ValueAtEnd);
            }
        }

        private static bool ValueChanged(IMeasurement measure)
        {
            return measure.ValueAtEnd != measure.ValueAtStart;
        }

        private void CreateElapsedMeasure()
        {
            elapsed = new ElapsedMicros();
            samples.Add(elapsed);
        }

        private void CreateCollectionMeasures()
        {
            gc2 = new GCCount(2);
            samples.Add(gc2);
            gc1 = new GCCount(1);
            samples.Add(gc1);
            gc0 = new GCCount(0);
            samples.Add(gc0);
        }

        private void CreateProcessorMeasurement()
        {
            processor = new Processor();
            samples.Add(processor);
        }

        private void CreatePerfCounterMeasures() 
        {
            string instance = Process.GetCurrentProcess().ProcessName;

            foreach (string catName in perfCounterCategories.Where(PerformanceCounterCategory.Exists))
            {
                PerformanceCounterCategory c = new PerformanceCounterCategory(catName);
                if (c.InstanceExists(instance))
                {
                    foreach (PerformanceCounter pc in c.GetCounters(instance))
                    {
                        if (IncludeCounter(pc)) samples.Add(new PerformanceCounterSample(pc));
                    }
                }
            }
        }

        private bool IncludeCounter(PerformanceCounter pc)
        {
            //Filter out the # gen x collections counters, as we have managed versions, see CreateCollectionMeasures
            return !(pc.CategoryName == NET_CLR_MEMORY && pc.CounterName.StartsWith("# Gen")) &&
                //and filter out any "Not Displayed" counters
                pc.CounterName != "Not Displayed";
        }

    }
}
