﻿using System;
using System.Diagnostics;

namespace Bismarck
{
    internal class PerformanceCounterSample : ISamplingMeasure
    {
        private PerformanceCounter pc;

        [DebuggerStepThrough]
        internal PerformanceCounterSample(PerformanceCounter counter)
        {
            pc = counter;
            Name = pc.CounterName;
            Category = pc.CategoryName;
        }

        [DebuggerStepThrough]
        public void SampleStart()
        {
            ValueAtStart = Convert.ToDecimal(pc.NextValue());
        }

        [DebuggerStepThrough]
        public void SampleEnd()
        {
            ValueAtEnd = Convert.ToDecimal(pc.NextValue());
            pc = null;
        }

        internal string Category 
        { 
            get; private set;
        }

        public string Name
        {
            get;
            private set;
        }

        public decimal ValueAtStart
        {
            get;
            private set;
        }

        public decimal ValueAtEnd
        {
            get;
            private set;
        }

    }
}
