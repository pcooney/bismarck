﻿Bismarck is a library for benchmarking .NET performance, particularly useful for micro-benchmarks.

The API works like this:

	//The function to be executed and measured
	Action someMethodToMeasure = MyMethod;

	//Method is executed and results stored in the return value
	IMeasurements m = Measure.Action(someMethodToMeasure);

	//m has convenient ToString() formatting for printing to the screen, as well as properties.
	Console.WriteLine(m);//or inspect details programmatically.

The Measure.Action records the before and after values of all .NET performance counters, as well as the high resolution
execution time of the measured Action and the executing core number. 

As much as possible, Bismarck avoids polluting the results with the execution time
of the framework itself.

