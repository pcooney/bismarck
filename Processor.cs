﻿using System.Runtime.InteropServices;

namespace Bismarck
{
    internal class Processor : ISamplingMeasure
    {
        [DllImport("kernel32.dll")]
        private static extern int GetCurrentProcessorNumber();

        public string Name
        {
            get { return "Physical processor number"; }
        }

        public decimal ValueAtStart
        {
            get;
            private set;
        }

        public decimal ValueAtEnd
        {
            get;
            private set;
        }

        public void SampleStart()
        {
            ValueAtStart = GetCurrentProcessorNumber();
        }

        public void SampleEnd()
        {
            ValueAtEnd = GetCurrentProcessorNumber();
        }
    }
}
