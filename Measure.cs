﻿using System;
using System.Diagnostics;

namespace Bismarck
{
    public static class Measure
    {
        /// <summary>
        /// Measures various aspects of the given action, including execution time and garbage collection.
        /// </summary>
        /// <param name="action">The action to measure</param>
        /// <param name="numberOfIterations">The number of times to invoke the action</param>
        /// <param name="preJit">If true, the action is invoked once before any measurement is done, so that the cost of JIT compilation
        /// is excluded from the measurement</param>
        /// <param name="preCollect">If true, garbage collection is triggered before any measurement is done, so that built up memory does
        /// not impact the measurements</param>
        /// <returns>An IMeasurements describing various measures of the action</returns>
        [DebuggerStepThrough]
        public static IMeasurements Action(Action action, uint numberOfIterations = 1, bool preJit = false, bool preCollect = false)
        {
            if (action == null) throw new ArgumentNullException("action");
            if (numberOfIterations == 0) throw new ArgumentException("numberOfIterations");

            if (preJit) action();
            if (preCollect) GC.Collect();

            ClrMeasurements m = new ClrMeasurements();
            m.SampleStart();

            //Call action numberOfIterations times
            for (uint i = 0; i < numberOfIterations; i++) action();
            
            m.SampleEnd();
            return m;
        }

    }
}